#!/usr/bin/make -f
# -*- makefile -*-

INSTALLDIR = $(CURDIR)/debian/tmp

_prefix = /usr
_bindir = $(_prefix)/bin
_sbindir = $(_prefix)/sbin
_libdir = $(_prefix)/lib
_includedir = $(_prefix)/include
_sysconfdir = /etc
_localstatedir = /var
_initrddir = $(_sysconfdir)/init.d
_datadir = $(_prefix)/share
_mandir = $(_datadir)/man
_docdir = $(_datadir)/doc
_archlib = $(shell perl -MConfig -e 'print $$Config{vendorarch}')

VERSION=1.13.0

%:
	dh $@ --with python2 --with python3

override_dh_auto_configure:
	chmod 644 security/globus_gsi_gss_constants.h \
		  security/globus_i_gsi_credential.h \
		  security/gssapi_openssl.h
	chmod 644 doc/lfc/INSTALL-*

	# LFC with mysql backend

	mkdir lfc-mysql
	cp -pr Imakefile Makefile.ini config configure VERSION scripts \
	    h lib shlib common ns rfio dpm \
	    srmv1 srmv2 srmv2.2 security dli dicomcopy dpmcopy \
	    test lfc-mysql

	# The code violates the strict aliasing rules all over the place...
	# Need to use -fnostrict-aliasing so that the -O2 optimization in
	# CFLAGS doesn't try to use them.
	sed 's!^CC +=!& $(CPPFLAGS) $(CFLAGS) -fno-strict-aliasing!' \
	    -i lfc-mysql/config/linux.cf

	sed 's!^CXXFLAGS =!& $(CPPFLAGS) $(CXXFLAGS)!' \
	    -i lfc-mysql/common/Imakefile

	cd lfc-mysql && \
	gsoapversion=`soapcpp2 -v < /dev/null 2>&1 | grep C++ | sed 's/.* //'` && \
	./configure lfc --with-mysql \
		--libdir=\'lib/$(DEB_HOST_MULTIARCH)\' \
		--with-gsoap-version=$$gsoapversion \
		--with-id-map-file=$(_sysconfdir)/lcgdm-mapfile \
		--with-ns-config-file=$(_sysconfdir)/NSCONFIG \
		--with-etc-dir='$$(prefix)/../etc' \
		--with-sysconf-dir=EtcDir/default \
		--with-initrc-dir=EtcDir/init.d \
		--with-emi \
		--without-argus

	sed 's!$$(prefix)/var!$$(prefix)/../var!' -i lfc-mysql/config/site.def

	cd lfc-mysql && \
	make -f Makefile.ini Makefiles

	# DPM with mysql backend

	mkdir dpm-mysql
	cp -pr Imakefile Makefile.ini config configure VERSION scripts \
	    h lib shlib common ns rfio dpm \
	    srmv1 srmv2 srmv2.2 security dli dicomcopy dpmcopy \
	    test dpm-mysql

	# The code violates the strict aliasing rules all over the place...
	# Need to use -fnostrict-aliasing so that the -O2 optimization in
	# CFLAGS doesn't try to use them.
	sed 's!^CC +=!& $(CPPFLAGS) $(CFLAGS) -fno-strict-aliasing!' \
	    -i dpm-mysql/config/linux.cf

	sed 's!^CXXFLAGS =!& $(CPPFLAGS) $(CXXFLAGS)!' \
	    -i dpm-mysql/common/Imakefile

	cd dpm-mysql && \
	gsoapversion=`soapcpp2 -v < /dev/null 2>&1 | grep C++ | sed 's/.* //'` && \
	./configure dpm --with-mysql \
		--libdir=\'lib/$(DEB_HOST_MULTIARCH)\' \
		--with-gsoap-version=$$gsoapversion \
		--with-dpm-config-file=$(_sysconfdir)/DPMCONFIG \
		--with-id-map-file=$(_sysconfdir)/lcgdm-mapfile \
		--with-ns-config-file=$(_sysconfdir)/DPNSCONFIG \
		--with-etc-dir='$$(prefix)/../etc' \
		--with-sysconf-dir=EtcDir/default \
		--with-initrc-dir=EtcDir/init.d \
		--with-emi \
		--without-argus

	sed 's!$$(prefix)/var!$$(prefix)/../var!' -i dpm-mysql/config/site.def

	sed -e '/^SECURITYDIR =/d' \
	    -e '/^COMMONDIR =/d' -i dpm-mysql/config/Project.tmpl
	sed -e 's/:.*(lcgdm).*/:/' -i dpm-mysql/lib/Imakefile
	sed -e 's/:.*(lcgdm).*/:/' -i dpm-mysql/shlib/Imakefile

	cd dpm-mysql && \
	make -f Makefile.ini Makefiles

override_dh_auto_build:
	cd lfc-mysql && \
	$(MAKE) SOAPFLG="$(shell pkg-config --cflags gsoap)" \
		DEB_HOST_MULTIARCH=$(DEB_HOST_MULTIARCH) \
		SYSCONFDIR=$(_sysconfdir) \
		LDFLAGS="$(LDFLAGS) -Wl,-z,defs" \
		SHLIBLDFLAGS="-shared $(LDFLAGS) -Wl,-z,defs"

	cd dpm-mysql/shlib && ln -s ../../lfc-mysql/shlib/liblcgdm.so* .

	cd dpm-mysql && \
	$(MAKE) SOAPFLG="$(shell pkg-config --cflags gsoap)" \
		DEB_HOST_MULTIARCH=$(DEB_HOST_MULTIARCH) \
		SYSCONFDIR=$(_sysconfdir) \
		LDFLAGS="$(LDFLAGS) -Wl,-z,defs" \
		SHLIBLDFLAGS="-shared $(LDFLAGS) -Wl,-z,defs"

	# Python3 modules
	mkdir python3 && cd python3 && \
	INCLUDE_PYTHON=`python3 -c "from distutils import sysconfig; \
		import sys; \
		sys.stdout.write('-I' + sysconfig.get_python_inc(0))"` && \
	PYTHON_LIB=`python3 -c "from distutils import sysconfig; \
		import sys; \
		sys.stdout.write('-lpython' + sys.version[:3] + \
		sys.abiflags if hasattr(sys, 'abiflags') else '' + ' ' + \
		sysconfig.get_config_var('LIBS') + ' ' + \
		sysconfig.get_config_var('SYSLIBS'))"` && \
	PYTHON_MODULE_SUFFIX=`python3 -c "from distutils import sysconfig; \
		print(sysconfig.get_config_var('SO'))"` && \
	for module in lfc lfcthr lfc2 lfc2thr ; do \
		swig -python -py3 -I../lfc-mysql/h -DNSTYPE_LFC \
			-outcurrentdir ../lfc-mysql/ns/$${module}.i ; \
		gcc $(CPPFLAGS) $(CFLAGS) -fno-strict-aliasing \
			-fPIC -D_LARGEFILE64_SOURCE -Dlinux \
			-pthread -DCTHREAD_LINUX -D_THREAD_SAFE -D_REENTRANT \
			-DPyInt_AsUnsignedLongLongMask=PyLong_AsUnsignedLongLongMask \
			-I../lfc-mysql/h -DNSTYPE_LFC $${INCLUDE_PYTHON} \
			-c -o $${module}_wrap.o $${module}_wrap.c ; \
		gcc $(LDFLAGS) -Wl,-z,defs \
			-shared -o _$${module}$${PYTHON_MODULE_SUFFIX} \
			$${module}_wrap.o $${PYTHON_LIB} \
			-L../lfc-mysql/shlib -llfc -llcgdm ; \
	done && \
	for module in dpm dpm2 ; do \
		swig -python -py3 -I../dpm-mysql/h -I../dpm-mysql/ns \
			-DNSTYPE_DPNS \
			-outcurrentdir ../dpm-mysql/dpm/$${module}.i ; \
		gcc $(CPPFLAGS) $(CFLAGS) -fno-strict-aliasing \
			-fPIC -D_LARGEFILE64_SOURCE -Dlinux \
			-pthread -DCTHREAD_LINUX -D_THREAD_SAFE -D_REENTRANT \
			-DPyInt_AsUnsignedLongLongMask=PyLong_AsUnsignedLongLongMask \
			-I../dpm-mysql/h -DNSTYPE_DPNS $${INCLUDE_PYTHON} \
			-c -o $${module}_wrap.o $${module}_wrap.c ; \
		gcc $(LDFLAGS) -Wl,-z,defs \
			-shared -o _$${module}$${PYTHON_MODULE_SUFFIX} \
			$${module}_wrap.o $${PYTHON_LIB} \
			-L../dpm-mysql/shlib -ldpm -llcgdm ; \
	done

override_dh_auto_clean:
	rm -rf lfc-mysql
	rm -rf dpm-mysql
	rm -rf python3

	rm -f debian/libdpm-perl.install debian/liblfc-perl.install

override_dh_auto_install:
	mkdir -p debian/tmp/etc/default

	# LFC with mysql backend

	cd lfc-mysql && \
	$(MAKE) SOAPFLG="$(shell pkg-config --cflags gsoap)" \
		DEB_HOST_MULTIARCH=$(DEB_HOST_MULTIARCH) \
		SYSCONFDIR=$(INSTALLDIR)$(_sysconfdir) \
		LDFLAGS="$(LDFLAGS) -Wl,-z,defs" \
		SHLIBLDFLAGS="-shared $(LDFLAGS) -Wl,-z,defs" \
		prefix=$(INSTALLDIR)$(_prefix) install install.man

	mv $(INSTALLDIR)$(_libdir)/$(DEB_HOST_MULTIARCH)/lfc-mysql \
	   $(INSTALLDIR)$(_libdir)/lfc-mysql

	# lfcdaemon startup script
	sed -e 's/LD_LIBRARY_PATH=$$LD_LIBRARY_PATH //' \
	    -e '/LD_LIBRARY_PATH/d' \
	    -e 's!\$$PREFIX/etc!/etc!' \
	    -e 's!mysqld!mysql!' \
	    -i $(INSTALLDIR)$(_sysconfdir)/lfc-mysql/lfcdaemon.init

	# lfcdaemon configuration file
	sed -e 's/\(RUN_LFCDAEMON=\).*/\1"yes"/' \
	    -e 's!/opt/lcg!!' -e 's!export SEMSGCONFIGFILE!#&!' \
	    -i $(INSTALLDIR)$(_sysconfdir)/lfc-mysql/lfcdaemon.conf

	# lfcdaemon man page
	sed -e 's!/opt/lcg!!g' \
	    -e 's/lfc-shutdown(1)/lfc-shutdown(8)/g' \
	    -i $(INSTALLDIR)$(_libdir)/lfc-mysql/lfcdaemon.8
	gzip -n -9 $(INSTALLDIR)$(_libdir)/lfc-mysql/lfcdaemon.8

	# lfc-shutdown man page
	gzip -n -9 $(INSTALLDIR)$(_libdir)/lfc-mysql/lfc-shutdown.8

	# lfc-dli startup script
	sed -e 's/LD_LIBRARY_PATH=$$LD_LIBRARY_PATH //' \
	    -e '/LD_LIBRARY_PATH/d' \
	    -e 's!\$$PREFIX/etc!/etc!' \
	    -e 's!/var/log/dli!/var/log/lfc-dli!g' \
	    -i  $(INSTALLDIR)$(_sysconfdir)/init.d/lfc-dli

	# lfc-dli configuration file
	sed -e 's!/var/log/dli!/var/log/lfc-dli!g' \
	    -e 's/\(LFC_HOST=\).*/\1`hostname -f`/' \
	    -e 's/\(RUN_DLIDAEMON=\).*/\1"yes"/' \
	    -i $(INSTALLDIR)$(_sysconfdir)/lfc-dli
	mv $(INSTALLDIR)$(_sysconfdir)/lfc-dli \
	   $(INSTALLDIR)$(_sysconfdir)/default/lfc-dli

	# lfc-dli log rotation configuration
	sed -e 's!/var/log/dli!/var/log/lfc-dli!g' \
	    -i $(INSTALLDIR)$(_sysconfdir)/logrotate.d/lfc-dli

	# lfc-dli man page
	sed -e 's/\(\.TH [^ ]* \)1/\18/' \
	    -e 's!/var/log/dli!/var/log/lfc-dli!g' \
	    -e 's/lfcdaemon(1)/lfc-server(8)/g' \
	    -i $(INSTALLDIR)$(_mandir)/man8/lfc-dli.8

	# Add the star accounting scripts
	mkdir $(INSTALLDIR)$(_datadir)/lcgdm/scripts
	install -m 755 scripts/StAR-accounting/star-accounting.py \
	    $(INSTALLDIR)$(_datadir)/lcgdm/scripts
	sed 's!/usr/bin/env python!/usr/bin/python!' \
	    -i $(INSTALLDIR)$(_datadir)/lcgdm/scripts/star-accounting.py

	# Add the LFC info provider script
	sed 's/\(^LFC_VERSION=\).*/\1$(VERSION)/' \
	    scripts/lcg-info-provider-lfc > \
	    $(INSTALLDIR)$(_datadir)/lcgdm/lcg-info-provider-lfc
	chmod 755 $(INSTALLDIR)$(_datadir)/lcgdm/lcg-info-provider-lfc

	# Create lfc user home and certificate directories
	mkdir -p $(INSTALLDIR)$(_localstatedir)/lib/lfc
	mkdir -p $(INSTALLDIR)$(_sysconfdir)/grid-security/lfcmgr

	# Remove due to name conflict
	rm $(INSTALLDIR)$(_mandir)/man3/log.3

	# DPM with mysql backend

	cd dpm-mysql && \
	$(MAKE) SOAPFLG="$(shell pkg-config --cflags gsoap)" \
		DEB_HOST_MULTIARCH=$(DEB_HOST_MULTIARCH) \
		SYSCONFDIR=$(INSTALLDIR)$(_sysconfdir) \
		LDFLAGS="$(LDFLAGS) -Wl,-z,defs" \
		SHLIBLDFLAGS="-shared $(LDFLAGS) -Wl,-z,defs" \
		prefix=$(INSTALLDIR)$(_prefix) install install.man

	mv $(INSTALLDIR)$(_libdir)/$(DEB_HOST_MULTIARCH)/dpm-mysql \
	   $(INSTALLDIR)$(_libdir)/dpm-mysql

	sed 's!/usr/bin/env python!/usr/bin/python!' \
	    -i $(INSTALLDIR)$(_bindir)/dpm-listspaces

	# dpm startup script
	sed -e 's/LD_LIBRARY_PATH=$$LD_LIBRARY_PATH //' \
	    -e '/LD_LIBRARY_PATH/d' \
	    -e 's!\$$PREFIX/etc!/etc!' \
	    -e 's!mysqld!mysql!' \
	    -i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpm.init

	# dpm configuration file
	sed -e 's/\(^DPNS_HOST=\).*/\1`hostname -f`/' \
	    -e 's/\(^RUN_DPMDAEMON=\).*/\1"yes"/' \
	    -e 's!/opt/lcg!!' -e 's!export SEMSGCONFIGFILE!#&!' \
	    -i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpm.conf

	# dpm man page
	sed -e 's/dpm-shutdown(1)/dpm-shutdown(8)/g' \
	    -i $(INSTALLDIR)$(_libdir)/dpm-mysql/dpm.8
	gzip -n -9 $(INSTALLDIR)$(_libdir)/dpm-mysql/dpm.8

	mv $(INSTALLDIR)$(_libdir)/dpm-mysql/NSCONFIG.templ \
	   $(INSTALLDIR)$(_libdir)/dpm-mysql/DPNSCONFIG.templ

	# dpm-shutdown man page
	gzip -n -9 $(INSTALLDIR)$(_libdir)/dpm-mysql/dpm-shutdown.8

	# dpm-buildfsv man page
	gzip -n -9 $(INSTALLDIR)$(_libdir)/dpm-mysql/dpm-buildfsv.8

	# dpnsdaemon startup script
	sed -e 's/LD_LIBRARY_PATH=$$LD_LIBRARY_PATH //' \
	    -e '/LD_LIBRARY_PATH/d' \
	    -e 's!\$$PREFIX/etc!/etc!' \
	    -e 's!mysqld!mysql!' \
	    -i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpnsdaemon.init

	# dpnsdaemon configuration file
	sed -e 's/\(^RUN_DPNSDAEMON=\).*/\1"yes"/' \
	    -e 's!/opt/lcg!!' -e 's!export SEMSGCONFIGFILE!#&!' \
	    -i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpnsdaemon.conf

	# dpnsdaemon man page
	sed -e 's!/opt/lcg!!g' \
	    -e 's!/etc/NSCONFIG!/etc/DPNSCONFIG!g' \
	    -e 's/dpns-shutown(1)/dpns-shutdown(8)/g' \
	    -i $(INSTALLDIR)$(_libdir)/dpm-mysql/dpnsdaemon.8
	gzip -n -9 $(INSTALLDIR)$(_libdir)/dpm-mysql/dpnsdaemon.8

	# dpns-shutdown man page
	gzip -n -9 $(INSTALLDIR)$(_libdir)/dpm-mysql/dpns-shutdown.8

	# dpmcopyd startup script
	sed -e 's/LD_LIBRARY_PATH=$$LD_LIBRARY_PATH //' \
	    -e '/LD_LIBRARY_PATH/d' \
	    -e 's!\$$PREFIX/etc!/etc!' \
	    -e 's!mysqld!mysql!' \
	    -i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpmcopyd.init

	# dpmcopyd configuration file
	sed -e 's/\(^DPNS_HOST=\).*/\1`hostname -f`/' \
	    -e 's/\(^DPM_HOST=\).*/\1`hostname -f`/' \
	    -e 's/\(^RUN_DPMCOPYDAEMON=\).*/\1"yes"/' \
	    -i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpmcopyd.conf

	# dpmcopyd man page
	gzip -n -9 $(INSTALLDIR)$(_libdir)/dpm-mysql/dpmcopyd.8

	for svc in srmv1 srmv2 srmv2.2 ; do \
	    sed -e 's/LD_LIBRARY_PATH=$$LD_LIBRARY_PATH //' \
		-e '/LD_LIBRARY_PATH/d' \
		-e 's!\$$PREFIX/etc!/etc!' \
		-e "s/$${svc}/dpm-$${svc}/g" \
		-e 's!mysqld!mysql!' \
		-i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/$${svc}.init ; \
	    mv $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/$${svc}.init \
	       $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpm-$${svc}.init ; \
	    sed -e "s/$${svc}/dpm-$${svc}/g" \
		-e 's/\(^DPNS_HOST=\).*/\1`hostname -f`/' \
		-e 's/\(^DPM_HOST=\).*/\1`hostname -f`/' \
		-e 's/\(^RUN_SRMV1DAEMON=\).*/\1"yes"/' \
		-e 's/\(^RUN_SRMV2DAEMON=\).*/\1"yes"/' \
		-i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/$${svc}.conf ; \
	    mv $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/$${svc}.conf \
	       $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpm-$${svc}.conf ; \
	    rmdir $(INSTALLDIR)$(_localstatedir)/log/$${svc} ; \
	    mkdir -p $(INSTALLDIR)$(_localstatedir)/log/dpm-$${svc} ; \
	    sed -e "s/$${svc}/dpm-$${svc}/g" \
		-i $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/$${svc}.logrotate ; \
	    mv $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/$${svc}.logrotate \
	       $(INSTALLDIR)$(_sysconfdir)/dpm-mysql/dpm-$${svc}.logrotate ; \
	    mv $(INSTALLDIR)$(_libdir)/dpm-mysql/$${svc} \
	       $(INSTALLDIR)$(_libdir)/dpm-mysql/dpm-$${svc} ; \
	    sed -e "s/$${svc}/dpm-$${svc}/g" \
		-e 's/dpm(1)/dpm(8)/g' \
		-i $(INSTALLDIR)$(_libdir)/dpm-mysql/$${svc}.8 ; \
	    mv $(INSTALLDIR)$(_libdir)/dpm-mysql/$${svc}.8 \
	       $(INSTALLDIR)$(_libdir)/dpm-mysql/dpm-$${svc}.8 ; \
	    gzip -n -9 $(INSTALLDIR)$(_libdir)/dpm-mysql/dpm-$${svc}.8 ; \
	done

	# rfiod startup script
	sed -e 's/LD_LIBRARY_PATH=$$LD_LIBRARY_PATH //' \
	    -e '/LD_LIBRARY_PATH/d' \
	    -e 's!\$$PREFIX/etc!/etc!' \
	    -e 's/rfiod/dpm-rfiod/g' \
	    -e 's!/var/log/rfio!/var/log/dpm-rfiod!g' \
	    -i $(INSTALLDIR)$(_sysconfdir)/init.d/rfiod
	mv $(INSTALLDIR)$(_sysconfdir)/init.d/rfiod \
	   $(INSTALLDIR)$(_sysconfdir)/init.d/dpm-rfiod

	# rfiod configuration file
	sed -e 's/rfiod/dpm-rfiod/g' \
	    -e 's!/var/log/rfio!/var/log/dpm-rfiod!g' \
	    -e 's/\(^DPNS_HOST=\).*/\1`hostname -f`/' \
	    -e 's/\(^DPM_HOST=\).*/\1`hostname -f`/' \
	    -e 's/\(^RUN_RFIOD=\).*/\1"yes"/' \
	    -i $(INSTALLDIR)$(_sysconfdir)/rfiod
	mv $(INSTALLDIR)$(_sysconfdir)/rfiod \
	   $(INSTALLDIR)$(_sysconfdir)/default/dpm-rfiod

	# rfiod log directory and log rotation configuration
	rmdir $(INSTALLDIR)$(_localstatedir)/log/rfio
	mkdir -p $(INSTALLDIR)$(_localstatedir)/log/dpm-rfiod
	sed -e 's!/var/log/rfio!/var/log/dpm-rfiod!g' \
	    -i $(INSTALLDIR)$(_sysconfdir)/logrotate.d/rfiod
	mv $(INSTALLDIR)$(_sysconfdir)/logrotate.d/rfiod \
	   $(INSTALLDIR)$(_sysconfdir)/logrotate.d/dpm-rfiod

	# rfiod binary and man page
	mv $(INSTALLDIR)$(_sbindir)/rfiod \
	   $(INSTALLDIR)$(_sbindir)/dpm-rfiod
	sed -e 's/rfiod/dpm-rfiod/g' \
	    -e 's!/usr/local/bin!/usr/sbin!g' \
	    -i $(INSTALLDIR)$(_mandir)/man8/rfiod.8
	mv $(INSTALLDIR)$(_mandir)/man8/rfiod.8 \
	   $(INSTALLDIR)$(_mandir)/man8/dpm-rfiod.8

	# Create dpm user home and certificate directories
	mkdir -p $(INSTALLDIR)$(_localstatedir)/lib/dpm
	mkdir -p $(INSTALLDIR)$(_sysconfdir)/grid-security/dpmmgr

	# Python3 modules
	PYTHON_SITEARCH=`python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))"` && \
	mkdir -p $(INSTALLDIR)$${PYTHON_SITEARCH} && \
	install -m 644 python3/*.py $(INSTALLDIR)$${PYTHON_SITEARCH} && \
	install python3/*.so $(INSTALLDIR)$${PYTHON_SITEARCH}

	# Dynamic paths in perl packages
	sed -e 's,@ARCHLIB@,$(_archlib),g' debian/libdpm-perl.install.in > debian/libdpm-perl.install
	sed -e 's,@ARCHLIB@,$(_archlib),g' debian/liblfc-perl.install.in > debian/liblfc-perl.install

override_dh_missing:
	dh_missing --fail-missing

override_dh_strip:
	dh_strip --dbgsym-migration='lcgdm-dbg (<< 1.9.0-3~)'
